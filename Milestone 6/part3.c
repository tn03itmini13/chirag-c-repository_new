#include<stdio.h>
#include <stdlib.h>
void mem_copy(void *dest, const void *src, unsigned int n);
int main()
{
	char* source = (char*)malloc(20*sizeof(char));
	char* destination = (char*)malloc(20*sizeof(char));
	int bits;
	printf("Enter the String: ");
	scanf("%s",source);
	printf("How many bits you wants to copy : ");
	scanf(" %d",&bits);
	mem_copy(destination,source,bits);
	printf("\nSource :");
	for(int i=0;i<=bits;i++)
	{
		printf("\n%d %c ",source+i,*(source+i));
	}
	printf("\n\nDestination :");
	for(int i=0;i<=bits;i++)
	{
		printf("\n%d %c ",destination+i,*(destination+i));
	}
}
void mem_copy(void *dest_p, const void *src_p, unsigned int n)
{
	char* dest = (char*)dest_p;
	char* src = (char*)src_p;
	int counter = 0;
	for(counter;counter<=(int)n;counter++)
	{
		dest[counter] = src[counter];		
	}
}