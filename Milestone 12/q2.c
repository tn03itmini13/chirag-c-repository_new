#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct dictionary {
	char* word;
	char* mean;
	struct dictionary* next;
};
struct dictionary* insert(struct dictionary* start,char* word,char* mean);
struct dictionary* insert_2(struct dictionary* start,char* word,char* mean);
void print(struct dictionary* node);
void search(struct dictionary* start,char* search_word);
int str_compare(char* a,char* b);
void delet(struct dictionary** start,char* delete_word);
void save(struct dictionary* start);
struct dictionary* load(struct dictionary* start);
int str_find_substringend(char *str1, char *str2,int pos);
int str_find_substringstart(char *str1, char *str2,int pos);
int main()
{
	char word[20];
	char mean[200];
	struct dictionary* start = NULL;
	
	int ch;
	while(ch!=7)
	{
	printf("\n1. Insert word\n2. Lookup word\n3. Remove a word\n4. print all\n5. save\n6. load\n7. Exit\nEnter choice : ");
	scanf("%d",&ch);
	switch(ch)
	{
		case 1 : 
					printf("Enter Word (max len 20) : ");
					scanf("%s",word);
					printf("Enter Meaning (max len 200) when coplete typing enter press 'tab' key to exit : \n");
					scanf("%[^\t]s",mean); 
					int i = 0;
					while(i!=sizeof(mean))
					{
						mean[i] = mean[i+1];
						i++;
					}
					start = insert(start,word,mean);
					break;
		
		case 2 :
					printf("Enter Word (max len 20) : ");
					scanf("%s",&word);
					search(start,word);
					break;
					
		case 3 : 
					printf("Enter Word (max len 20) : ");
					scanf("%s",&word);
					delet(&start,word);
					break;
					
		case 4 : 
					print(start);
					break;
					
		case 5 : 
					save(start);
					break;
		
		case 6 : 
					start = load(start);
					break;
					
		case 7 : exit;
	}
	}
	
}
void save(struct dictionary* start)
{
	FILE* fp = fopen("info.txt","w");
	if(start==NULL)
	{
		printf("\nEmpty!!");
	}
	else
	{
		struct dictionary* start_2 = start;
		int i =1;
		while(start_2!=NULL)
		{
			fputs("word :",fp);
			fputs(start_2->word,fp);
			fputs(" >",fp);
			fputs("\nmean :",fp);
			fputs(start_2->mean,fp);
			fputs(" >",fp);
			fputs("\n\n",fp);
			start_2 = start_2->next;
			i++;
		}
	}
	fclose(fp);
	
}
struct dictionary* load(struct dictionary* start)
{
	struct dictionary* start_2 = start;
	FILE* fp = fopen("info.txt","r");
	fseek(fp,0,SEEK_END);
	int size = ftell(fp);
	fseek(fp,0,SEEK_SET);
	char file[size];
	fread(file,1,size,fp);
	int i=0,counter=0;
	while(counter != size)
	{
		if(file[counter]=='>')
		{
			i=counter;
		}
		counter++;
	}
	file[i+1]='\0';
	i=0;
	int pos_start=0,pos_end=0;
	char word[20];
	char mean[200];
	
	while(file[pos_end+3]!='\0')
	{
		pos_start = str_find_substringstart(file,"word :",pos_end);
		pos_end = str_find_substringend(file," >",pos_start);
		int j=0;
		for(int i=pos_start;i<=pos_end;i++)
		{
			word[j] = file[i];
			j++;
		}
		word[j]='\0';
		pos_start = str_find_substringstart(file,"mean :",pos_end);
		pos_end = str_find_substringend(file," >",pos_start);
		
		j=0;
		for(int i=pos_start;i<=pos_end;i++)
		{
			mean[j] = file[i];
			j++;
		}
		mean[j]='\0';
		start_2 = insert(start_2,word,mean);
	}
	
	fclose(fp);
	return start_2;
}

int str_find_substringstart(char *str1, char *str2,int pos)
{
	int i=pos,j=0,str2_len=0,count=0,start=-1;
	while(str2[j]!='\0')
	{
		str2_len++;
		j++;
	}
	j=0;
	while(str1[i]!='\0' && str2[j]!='\0')
	{
		if(str1[i]==str2[j])
		{
			if(start==-1)
			{
				start=i;
			}
			count++;
			i++;
			j++;
		}
		else
		{
			i++;
			if(str2[j]!='\0')
			{
			j=0;
			start=-1;
			count=0;
			}
			
		}
	}
	if(count==str2_len){
		return start+str2_len;
	}
	else
	{return -1;}
}
int str_find_substringend(char *str1, char *str2,int pos)
{
	
	int i=pos,j=0,str2_len=0,count=0,start=0;
	while(str2[j]!='\0')
	{
		str2_len++;
		j++;
	}
	j=0;
	while(str1[i]!='\0')
	{
		if(str1[i]==str2[j])
		{
			if(start==0){start=i;}
			count++;
			i++;
			j++;
		}
		else
		{
			i++;
			if(str2[j]!='\0')
			{
			j=0;
			start=0;
			count=0;
			}
			
		}

	}
	if(count==str2_len){
		return start-1;
	}
	else
	{
		return -1;}
}
void print(struct dictionary* node)
{
	if(node==NULL)
	{
		printf("\nEmpty!!");
	}
	else
	{
		struct dictionary* start_2 = node;
		printf("\nDictionary Data :");
		int i =1;
		while(start_2!=NULL)
		{
			printf("\n %d) %s : %s",i,start_2->word,start_2->mean);
			start_2 = start_2->next;
			i++;
		}
	}
}
void search(struct dictionary* start,char* search_word)
{
	
	if(start==NULL)
	{
		printf("\nEmpty!!");
	}
	else
	{
		struct dictionary* start_2 = start;
		printf("\nDictionary Data :");
		while(start_2!=NULL)
		{
			if(str_compare(start_2->word,search_word)==0)
			{
				printf("\n%s : %s",start_2->word,start_2->mean);
				break;
			}
			start_2 = start_2->next;
		}
	}
}

void delet(struct dictionary** start,char* delete_word)
{
	if(start==NULL)
	{
		printf("\nEmpty!!");
	}
	else
	{
		struct dictionary* start_2 = (*start);
		struct dictionary* pre = NULL;
		printf("\nDictionary Data :");
		if(str_compare(start_2->word,delete_word)==0)
		{
			printf("%s : %s",start_2->word,start_2->mean);
			(*start) = (*start)->next;
			free(start_2);
		}
		else
		{
			while(start_2!=NULL)
			{
				if(str_compare(start_2->word,delete_word)==0)
				{
					pre->next = start_2->next;
					free(start_2);
					break;
				}
				pre = start_2;
				start_2 = start_2->next;
				
			}	
		}
	}
}

int str_compare(char* a,char* b)
{
	while(*a!='\0' && *b!='\0')
	{
		if(*a<*b)
		{
			return -1;
			break;
		}
		else if(*a>*b)
		{
			return 1;
			break;
		}
		else
		{
			a = a+1;
			b = b+1;
		}
	}
	return 0;
}
struct dictionary* insert(struct dictionary* start,char* word,char* mean)
{
	
	char* word_m = (char*)malloc(sizeof(word));
	char* mean_m = (char*)malloc(sizeof(mean));
	strcpy(word_m,word);
	strcpy(mean_m,mean);
	
	struct dictionary* temp = (struct dictionary*)malloc(sizeof(struct dictionary));
	struct dictionary* start_2 = NULL;
	struct dictionary* pre = NULL;
	temp->word = word_m;
	temp->mean = mean_m;
	temp->next = NULL;
	
	if(start==NULL)
	{
		start = temp;
	}
	else
	{
		start_2 = start;
		while(start_2!=NULL)
		{
			if(str_compare(start_2->word,temp->word)!=-1)
			{
				temp->next = pre->next;
				pre->next = temp;
				return start;
				break;
			}
			else
			{
				if(start_2->next==NULL)
				{
					start_2->next = temp;
					return start;
				}
				else
				{
					pre = start_2;
					start_2 = start_2->next;	
				}
			}
			
		}
	}
	return start;
}
