#include<stdio.h>
int is_prime(int n);
void factor(int n);

int main()
{
	int num;
	printf("Enter a number : ");
	scanf("%d",&num);
	printf("Prime factors of %d is : ",num);
	factor(num);
}

void factor(int n)
{
	int i = n;
	while(i>0)
	{
		if(n%i==0)
		{
			if(is_prime(i))
			{ 
                                                                    printf(" %d",i);
                                                                 }
		}
		i--;
	}
}

int is_prime(int n)
{
	int ans, i=2,flag;
	while(i<n)
	{
		if(n%i==0)
		{
			flag = 0;
			break;
		}
		else
		{
			flag = 1;
		}
		i++;
	}
	
	return flag;
}
