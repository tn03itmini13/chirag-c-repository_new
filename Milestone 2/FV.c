#include<stdio.h>
double FV(double rate, unsigned int nperiods, double PV);
int main()
{
	double rate,pv,fv;
	unsigned int np;
	
	printf("Enter rate : ");
	scanf("%lf",&rate);
	printf("Enter nperiods : ");
	scanf("%d",&np);
	printf("Enter pv : ");
	scanf("%2lf",&pv);
	
	fv = FV(rate,np,pv);
	printf("Future Value : %2lf",fv);
	
	return 0;
}

double FV(double rate, unsigned int nperiods, double pv)
{
	double fv;

	fv = pv * (1+ rate)*nperiods;
	
	return fv;
}
