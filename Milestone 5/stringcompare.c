#include<stdio.h>
int string_compare(char *str1, char *str2);
int main()
{
    char s1[20], s2[20];
    int result;
    printf("Enter 1st string:");
    scanf("%s",&s1);
    printf("Enter 2nd string:");
    scanf("%s",&s2);
    result= string_compare(s1,s2);
    printf("Result: %d",result);
}
int string_compare(char *str1, char *str2)
{
    int sum1=0, sum2=0;
    while(*str1 != '\0')
    {
        sum1 = sum1+*str1;
        str1++;
    }
    while(*str2 != '\0')
    {
        sum2 = sum2+*str2;
        str2++;
    }
    if (sum1==sum2)
    {
        return 0;
    }
    else if (sum1<sum2)
    {
        return -1;
    }
    else
    {
        return 1;
    }
}