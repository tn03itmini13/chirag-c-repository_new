#include<stdio.h>
void integer();
void floats();
void character();
void main()
{
    int n,ch;
    void (*ptr)();
    printf("\n 1.Integer \n 2.Float \n 3.Char \n Select any one data type to proceed: ");
    scanf("%d",&ch);
    switch(ch)
    {
        case 1: ptr=integer;
                ptr();
                break;
        
        case 2: ptr=floats;
                ptr();
                break;

        case 3: ptr=character;
                ptr();
                break;

        default: printf("Enter a valid choice!!");
                 break;
    }
}

void integer()
{
    int chirag[5];
    int n,a,temp;
    printf("Enter total number of integers to perform operation on (upto 5): ");
    scanf("%d",&n);
    for(int i=0; i<n; i++){
        printf("\n Enter integer value to add at index %d in array:");
        scanf("%d",&chirag[i]);
    }
    printf("\n How do you want to sort the array? \n 1.Ascending \n 2.Descending :");
    scanf("%d",&a);
    if(a==1)
    {
        for(int i=0;i<n;i++)
        {
            for(int j=i;j<n;j++)
            {
                if(chirag[i]>chirag[j])
                {
                    temp=chirag[i];
                    chirag[i]=chirag[j];
                    chirag[j]=temp;
                }
            }
        }
        for(int i=0;i<n;i++)
			{
				printf("\n At index--> %d Element is:%d \n",i,chirag[i]);
				
			}
    }
    else if(a==2)
    {
        for(int i=0;i<n;i++)
        {
            for(int j=i;j<n;j++)
            {
                if(chirag[i]<chirag[j])
                {
                    temp=chirag[i];
                    chirag[i]=chirag[j];
                    chirag[j]=temp;
                }
            }
        }
        for(int i=0;i<n;i++)
			{
				printf("\n At index--> %d Element is:%d \n",i,chirag[i]);
				
			}
    }
    else
    {
        printf("Enter a valid choice!!");
    }
}

void floats()
{
    int chirag[5];
    int n,a,temp;
    printf("Enter total numbers to perform operation on (upto 5): ");
    scanf("%d",&n);
    for(int i=0; i<n; i++){
        printf("Enter float value to add at address %d:\t");
        scanf(" %f",&chirag[i]);
    }
    printf("\n How do you want to sort the array? \n 1.Ascending \n 2.Descending :");
    scanf("%d",&a);
    if(a==1)
    {
        for(int i=0; i<n; i++)
        {
            for(int j=i;j,n;j++)
            {
                if(chirag[i]>chirag[j])
                {
                    temp=chirag[i];
                    chirag[i]=chirag[j];
                    chirag[j]=temp;
                }
            }
        }
        for(int i=0;i<n;i++)
			{
				printf("\n At index--> %d Element is:%f \n",i,chirag[i]);	
			}
    }
    else if(a==2)
    {
        for(int i=0;i<n;i++)
        {
            for(int j=i;j<n;j++)
            {
                if(chirag[i]<chirag[j])
                {
                    temp=chirag[i];
                    chirag[i]=chirag[j];
                    chirag[j]=temp;
                }
            }
        }
        for(int i=0;i<n;i++)
			{
				printf("\n At index--> %d Element is:%f \n",i,chirag[i]);
			}
    }
    else
    {
        printf("Enter a valid choice!!");
    }
}

void character()
{
    int chirag[5];
    int n,temp;
    printf("\n Enter total characters to perform operation on (upto 5): \n");
    scanf("%d",&n);
    for(int i=0;i<n;i++)
    {
        printf("\n Enter %d character values to be added in array:\t",i);
        scanf(" %c",&chirag[i]);
    }
    for(int i=0;i<n;i++)
    {
        for(int j=i;j<n;j++)
        {
            if(chirag[i]<chirag[j])
            {
                temp=chirag[i];
                chirag[i]=chirag[j];
                chirag[j]=temp;
            }
        }
    }
    for(int i=0;i<n;i++)
			{
				printf("\n At index--> %d character is:%c \n",i,chirag[i]);	
			}
}