#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct dictionary {
	char* word;
	char* mean;
	struct dictionary* next;
};
struct dictionary* insert(struct dictionary* start);
void print(struct dictionary* node);
void search(struct dictionary* start,char* search_word);
int str_compare(char* a,char* b);
void delet(struct dictionary** start,char* delete_word);
int main()
{
	char word[50];
	struct dictionary* start = NULL;
	
	int ch;
	while(ch!=5)
	{
	printf("\n1. Insert word\n 2. Lookup word\n 3. Remove a word\n 4. print all\n 5.Exit\n  Enter choice : ");
	scanf("%d",&ch);
	switch(ch)
	{
		case 1 : 
					start = insert(start);
					break;
		
		case 2 :
					printf("Enter Word (max char 50) : ");
					scanf("%s",&word);
					search(start,word);
					break;
					
		case 3 : 
					printf("Enter Word (max char 50) : ");
					scanf("%s",&word);
					delet(&start,word);
					break;
					
		case 4 : 
					print(start);
					break;
					
		case 5 : exit;
	}
	}
	
}
struct dictionary* insert(struct dictionary* start)
{
	char word[50];
	char mean[300];
	printf("Enter Word (max len 20) : ");
	scanf("%s",word);
	printf("Enter Meaning (max char len 300) when finished typing enter press 'tab' key to exit: \n");
	scanf(" %s",mean); 
	int i = 0;
	while(i!=sizeof(mean))
	{
		mean[i] = mean[i+1];
		i++;
	}
	char* word_m = (char*)malloc(sizeof(word));
	char* mean_m = (char*)malloc(sizeof(mean));
	strcpy(word_m,word);
	strcpy(mean_m,mean);
	printf("%s : %s",word_m,mean_m);
	
	struct dictionary* temp = (struct dictionary*)malloc(sizeof(struct dictionary));
	struct dictionary* start2 = NULL;
	struct dictionary* pre = NULL;
	temp->word = word_m;
	temp->mean = mean_m;
	temp->next = NULL;
	if(start==NULL)
	{
		start = temp;
	}
	else
	{
		start2 = start;
		while(start2!=NULL)
		{
			if(str_compare(start2->word,temp->word)!=-1)
			{
				temp->next = pre->next;
				pre->next = temp;
				return start;
				break;
			}
			else
			{
				if(start2->next==NULL)
				{
					start2->next = temp;
					return start;
				}
				else
				{
					pre = start2;
					start2 = start2->next;	
				}
			}
			
		}
	}
	return start;
}

void print(struct dictionary* node)
{
	if(node==NULL)
	{
		printf("\n Dictionary Empty!!");
	}
	else
	{
		struct dictionary* start2 = node;
		printf("\nDictionary Data :");
		int i =1;
		while(start2!=NULL)
		{
			printf("\n %d) %s : %s",i,start2->word,start2->mean);
			start2 = start2->next;
			i++;
		}
	}
}
void search(struct dictionary* start,char* search_word)
{
	
	if(start==NULL)
	{
		printf("\nEmpty!!");
	}
	else
	{
		struct dictionary* start2 = start;
		printf("\nDictionary Data :");
		while(start2!=NULL)
		{
			if(str_compare(start2->word,search_word)==0)
			{
				printf("\n%s : %s",start2->word,start2->mean);
				break;
			}
			start2 = start2->next;
		}
	}
}

void delet(struct dictionary** start,char* delete_word)
{
	if(start==NULL)
	{
		printf("\nEmpty!!");
	}
	else
	{
		struct dictionary* start2 = (*start);
		struct dictionary* pre = NULL;
		printf("\nDictionary Data :");
		if(str_compare(start2->word,delete_word)==0)
		{
			printf("%s : %s",start2->word,start2->mean);
			(*start) = (*start)->next;
			free(start2);
		}
		else
		{
			while(start2!=NULL)
			{
				if(str_compare(start2->word,delete_word)==0)
				{
					pre->next = start2->next;
					free(start2);
					break;
				}
				pre = start2;
				start2 = start2->next;
				
			}	
		}
	}
}

int str_compare(char* a,char* b)
{
	while(*a!='\0' && *b!='\0')
	{
		if(*a<*b)
		{
			return -1;
			break;
		}
		else if(*a>*b)
		{
			return 1;
			break;
		}
		else
		{
			a = a+1;
			b = b+1;
		}
	}
	return 0;
}