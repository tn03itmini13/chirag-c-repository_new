#include<stdio.h>
#include<stdlib.h>
#include<math.h>
struct Node {
	int data;
	struct Node* next;
};
struct Node* insert(struct Node* head,int val);
void print(struct Node* head);
void count(struct Node* head,int x);
void getNth(struct Node* head,int pos);
struct Node* deleteList(struct Node* head);
void pop(struct Node** head);
void insertNth(struct Node* head,int pos,int val);
struct Node* sortedInsert(struct Node* head,int val);
void insertSort(struct Node* head);
void append(struct Node** Aptr,struct Node** Bptr);
void FrontBackSplit(struct Node* source,struct Node** frontRef, struct Node** backRef);
void RemoveDuplicates(struct Node* head);
void MoveNode(struct Node** destRef, struct Node** sourceRef);
void AlternatingSplit(struct Node* source,struct Node** aRef, struct Node** bRef);
struct Node* ShuffleMerge(struct Node* aRef, struct Node* bRef);
struct Node* SortedMerge(struct Node* a, struct Node* b);
void MergeSort(struct Node* headRef);
struct Node* SortedIntersect(struct Node* a, struct Node* b);
void reverse(struct Node* a);

int main()
{
	struct Node* head;
	struct Node* head1;
	struct Node* head2;
	head = NULL;
	head1 = NULL;
	head2 = NULL;
	int ch;
	int n,ele,pos;
	while(ch!=20)
	{
		printf("\n\n1. Insert\n2. Print\n3. Count\n4. getNth\n5. deleteList\n6. pop\n7. insertNth\n8. sortedInsert\n9. insertSort\n
		10. append\n11. FrontBackSplit\n12. RemoveDuplicates\n13. MoveNode\n14. AlternatingSplit\n15. ShuffleMerge\n16. SortedMerge\n
		17. MergeSort\n18. SortedIntersect\n19. reverse\nEnter Choice : ");
		scanf("%d",&ch);
		switch(ch)
		{
			case 1 : 
						printf("\nHow many numbers do you want to enter: ");
						scanf("%d",&n);
						printf("Enter Elements: ");
						for(int i=0;i<n;i++)
						{
							scanf("%d",&ele);
							head = insert(head,ele);
						}
						break;
			case 2 : 
						print(head);print(head1);print(head2);
						break;
	
			case 3 :
						printf("\nEnter element to count : ");
						scanf("%d",&ele);
						count(head,ele);
						break;
			case 4 :
						printf("\nEnter position : ");
						scanf("%d",&pos);
						getNth(head,pos);
						break;
			case 5 :
						head = deleteList(head);
						break;
			case 6 :
						pop(&head);
						break;
			case 7 :
						printf("\nEnter position : ");
						scanf("%d",&pos);
						printf("Enter Element : ");
						scanf("%d",&ele);
						insertNth(head,pos,ele);
						break;
			case 8 : 
						printf("Enter Elements : ");
						scanf("%d",&ele);
						head = sortedInsert(head,ele);
						break;
			case 9 :
						insertSort(head);
						break;
			case 10 :
						printf("\nHow many numbers you wants to enter : ");
						scanf("%d",&n);
						printf("Enter Elements : ");
						for(int i=0;i<n;i++)
						{
							scanf("%d",&ele);
							head1 = insert(head1,ele);
						}
						append(&head,&head1);
						break;
			case 11 :
						FrontBackSplit(head,&head1,&head2);
						break;
			case 12 :
						RemoveDuplicates(head);
						break;
			case 13 :
						print(head);print(head1);print(head2);
						printf("\nHow meny numbers you wants to enter : ");
						scanf("%d",&n);
						printf("Enter Elements : ");
						for(int i=0;i<n;i++)
						{
							scanf("%d",&ele);
							head1 = insert(head1,ele);
						}
						MoveNode(&head,&head1);
						break;
			case 14 :
						AlternatingSplit(head,&head1,&head2);
						break;
			case 15 :
						print(head);print(head1);print(head2);
						printf("\nHow meny numbers you wants to enter : ");
						scanf("%d",&n);
						printf("Enter Elements : ");
						for(int i=0;i<n;i++)
						{
							scanf("%d",&ele);
							head1 = insert(head1,ele);
						}
						head2 = ShuffleMerge(head,head1);
						break;
			case 16 :
						print(head);print(head1);print(head2);
						printf("\nHow meny numbers you wants to enter : ");
						scanf("%d",&n);
						printf("Enter Elements : ");
						for(int i=0;i<n;i++)
						{
							scanf("%d",&ele);
							head1 = insert(head1,ele);
						}
						head2 = SortedMerge(head,head1);
						break;
			case 17 :
						MergeSort(head);
						break;
			case 18 :
						print(head);print(head1);print(head2);
						printf("\nHow meny numbers you wants to enter : ");
						scanf("%d",&n);
						printf("Enter Elements : ");
						for(int i=0;i<n;i++)
						{
							scanf("%d",&ele);
							head1 = insert(head1,ele);
						}
						head2 = SortedIntersect(head,head1);
						break;
			case 19 :
						reverse(head);
						break;
			case 20 : 
						exit;
						break;
		}
	}
}
void print(struct Node* head)
{
	if(head==NULL)
	{printf("\nlist is Empty !!!");}
	else
	{
		struct Node* head_2 = head;
		printf("\nList : ");
		while(head_2!=NULL)
		{
			printf("%d ",head_2->data);
			head_2 = head_2->next;
		}
	}
}
struct Node* insert(struct Node* head,int val)
{
	
	struct Node* temp = (struct Node*)malloc(sizeof(struct Node));
	temp->data = val;
	temp->next = NULL;
	
	if(head==NULL)
	{
		head=temp;
	}
	else
	{
		struct Node* head_2 = head;
		while(head_2->next!=NULL)
		{
			head_2 = head_2->next;
		}
		head_2->next = temp;
	}
	return head;
}
void count(struct Node* head,int x)
{
	int counter=0;
	struct Node* head_2 = head;
	while(head_2!=NULL)
	{
		if(head_2->data==x)
		{
			counter++;
		}
		head_2 = head_2->next;
	}
	printf("\n%d present in list %d times.",x,counter);
}

void getNth(struct Node* head,int pos)
{
	struct Node* head_2 = head;
	int flag=0;
	for(int i=0;i<pos;i++)
	{
		if(head_2->next==NULL)
		{
			flag=1;
			break;
		}
		head_2 = head_2->next;
	}
	if(flag==0)
	{printf("\n%d is present at %d possition",head_2->data,pos);}

}

struct Node* deleteList(struct Node* head)
{
	
	if(head==NULL)
	{printf("\nlist is Empty !!!");}
	else
	{
		struct Node* current = head;
		struct Node* pre = NULL;
			while(current!=NULL)
			{
				pre = current;
				current = current->next;
				free(pre);
			}
		printf("\nList Deletion Complete.");
		head = NULL;
		return head;
	}
}

void pop(struct Node** head)
{
	if(head==NULL)
	{printf("\nlist is Empty !!!");}
	else
	{
		struct Node** head_2 = head;
		(*head) = (*head)->next;
		free(head_2);
	}
}

void insertNth(struct Node* head,int pos,int val)
{
	struct Node* temp = (struct Node*)malloc(sizeof(struct Node));
	temp->data = val;
	temp->next = NULL;
	
	struct Node* head_2 = head;
	for(int i=0;i<pos-1;i++)		
	{
		head_2 = head_2->next;
	}
	temp->next = head_2->next;
	head_2->next = temp;
	printf("\n%d is inserted at %d possition.",temp->data,pos);
}

struct Node* sortedInsert(struct Node* head,int val)
{
	struct Node* temp = (struct Node*)malloc(sizeof(struct Node));
	temp->data = val;
	temp->next = NULL;
	
	if(head==NULL)
	{printf("\nlist is Empty !!!");}
	else
	{
		struct Node* i = head;
		while(i!=NULL)
		{
			struct Node* j = i->next;
			while(j!=NULL)
			{
				if(i->data > j->data)
				{
					int temp;
					temp = i->data;
					i->data = j->data;
					j->data = temp;
				}
				j = j->next;
			}
			i = i->next;
		}
		
		if(temp->data < head->data)
		{
			temp->next = head;
			head = temp;
		}
		else
		{
			struct Node* current = head;
			struct Node* pre = current;
			while(current!=NULL)
			{
				if(current->data > temp->data)
				{
					temp->next = current;
					pre->next = temp;
					break;
				}
				pre = current;
				current = current->next;
			}
		}
		
		return head;
	}
	
}
void insertSort(struct Node* head)
{
	if(head==NULL)
	{printf("\nlist is Empty !!!");}
	else
	{
		struct Node* i = head;
		while(i!=NULL)
		{
			struct Node* j = i->next;
			while(j!=NULL)
			{
				//printf("\n%d == %d",i->data,j->data);
				if(i->data > j->data)
				{
					int temp;
					temp = i->data;
					i->data = j->data;
					j->data = temp;
				}
				j = j->next;
			}
			i = i->next;
		}
	}
}

void append(struct Node** Aptr,struct Node** Bptr)
{
	struct Node* A = (*Aptr);
	struct Node** B = (Bptr);
	
	while(A->next!=NULL)
	{
		A=A->next;
	}
	A->next = (*B);
	
	(*B) = NULL;
	free((*B));
}

void FrontBackSplit(struct Node* source,struct Node** frontRef, struct Node** backRef)
{
	float len=0;
	
	struct Node* src = source;
	(*frontRef)=source;
	while(src!=NULL)
	{
		len++;
		src=src->next;
	}
	int lenf =  (int)round(len/2);

	for(int i=1;i<lenf;i++)
	{
		source=source->next;
	}
	(*backRef) = source->next;
	source->next = NULL;
}

void RemoveDuplicates(struct Node* head)
{
	insertSort(head);
	
	struct Node* first = head;
	struct Node* second = head->next;
	struct Node* temp=NULL;

		while(second!=NULL)
		{			
			if(first->data == second->data)
			{
				first->next = second->next;
				second = second->next;
			}
			else
			{
				first = second;
				second = second->next;
			}
			
		}
}

void MoveNode(struct Node** destRef, struct Node** sourceRef)
{
	struct Node* temp;
	temp = (*sourceRef);
	(*sourceRef) = (*sourceRef)->next;
	temp->next = (*destRef);
	(*destRef) = temp;
}

void AlternatingSplit(struct Node* source,struct Node** aRef, struct Node** bRef)
{
	int temp = 0,flag=-1;
		(*aRef) = source;
	struct Node* A = (*aRef);
	source = source->next;
	(*bRef) = source;
	struct Node* B = (*bRef);
	source = source->next;
	while(source!=NULL)
	{		
		if(temp==0)
		{
			A->next = source;
			A = A->next;
			source = source->next;
			temp = 1;
			
		}
		else if(temp==1)
		{
			B->next = source;
			B = B->next;
			source = source->next;
			temp = 0;			
		}
	}
		if(temp==1)
		{
			B->next=NULL;
		}
		else if(temp==0)
		{
			A->next=NULL;
		}
	
}

struct Node* ShuffleMerge(struct Node* aRef, struct Node* bRef)
{
	int flag=0;
	struct Node* a = aRef;
	struct Node* b = bRef;
	struct Node* source = a;
	struct Node* head = source;
	a = a->next;
	while(a!=NULL || b!=NULL)
	{
	if(flag==1 && a!=NULL)
	{
		source->next = a;
		a = a->next;
		source = source->next;
		flag = 0;
	}
	else if(flag==0 && b!=NULL)
	{
		source->next = b;
		b = b->next;
		source = source->next;
		flag = 1;
	}
	else if(a==NULL)
	{
		source->next = b;
		b = b->next;
		source = source->next;
	}
	else if(b==NULL)
	{
		source->next = a;
		a = a->next;
		source = source->next;
	}
	}
	return head;
}

struct Node* SortedMerge(struct Node* a, struct Node* b)
{
	struct Node* temp;
	struct Node* head;
	if(a->data < b->data)
	{
		head = a;
		a = a->next;
	}
	else
	{
		head = b;
		b = b->next;
	}
	temp = head;
	while(a!=NULL || b!=NULL)
	{
		
		if(a==NULL)
		{
			temp->next = b;
			return head;
		}
		else if(b==NULL)
		{
			temp->next = a;
			return head;
		}
		else
		{
			if(a->data < b->data)
			{
				temp->next = a;
				temp = temp->next;
				a = a->next;
				//print(temp);
			}
			else if(b->data < a->data)
			{
				temp->next = b;
				temp = temp->next;
				b = b->next;
			}
		}
	}
}

void MergeSort(struct Node* headRef)
{
	
struct Node* left = NULL;
struct Node* right = NULL;
while(left->next!=NULL && right->next!=NULL)
{
FrontBackSplit(headRef,&left,&right);
MergeSort(left);
MergeSort(right);
}
}

struct Node* SortedIntersect(struct Node* a, struct Node* b)
{
	struct Node* temp = NULL;
	while(a!=NULL && b!=NULL)
	{
		if(a->data == b->data)
		{
			temp = insert(temp,a->data);
			a = a->next;
			b = b->next;
		}
		else if(a->data < b->data)
		{
			a = a->next;
		}
		else if(b->data < a->data)
		{
			b = b->next;
		}
	}
	return temp;
	
	
}

void reverse(struct Node* a)
{
	struct Node* head = NULL;
	struct Node* pre = NULL;
	struct Node* current = a;
	struct Node* Next = a->next;
	while(Next!=NULL)
	{
		current->next = pre;
		pre = current;
		current = Next;
		head = current;
		Next = Next->next;
	}
	current->next = pre;
	print(head);
}